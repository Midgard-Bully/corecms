<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-plus-circle"></i> Add News</div>
            <div class="card-body">
                <?php
                if(isset($_POST['addnews']))
                {
                    $authorid = $_SESSION['acp'];
                    $newsheader = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['newsheader']));
                    $newstext = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['newstext']));
                    $authordb = $mysqliA->query("SELECT * FROM web_user WHERE id = $authorid") or die (mysqli_error($mysqliA));
                    $res_auth = $authordb->fetch_assoc();
                    $author = $res_auth['username'];

                    if(empty($newsheader && $newstext))
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Please check fields!
                            </div>
                         ';
                        header("refresh:3; url=$custdir/acp/news-add.php");
                    }
                    else
                    {
                            //insert
                        $news_add = $mysqliA->query("INSERT INTO `web_news` (`header`, `news`, `author`) VALUES ('$newsheader', '$newstext', '$author')") or die (mysqli_error($mysqliA));
                        if($news_add === true) {
                            echo '
                                <div class="alert alert-success" role="alert">
                                    <i class="fad fa-check-circle"></i> Item was added
                                </div>
                            ';
                            header("refresh:3; url=$custdir/acp/view-news.php");
                        }
                    }
                }
                else
                {
                    ?>
                    <form name="addnews" method="post">
                        <div class="form-group">
                            <label for="newsheader">News Header</label>
                            <input type="text" name="newsheader" class="form-control" required="required">
                            <small>Max length 45 characters</small>
                            <?php echo $_SESSION['acp']; ?>
                        </div>
                        <div class="form-group">
                            <label for="newstext">News Text</label>
                            <input type="text" name="newstext" class="form-control" required="required">
                            <small>Enter a news text</small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" name="addnews"><i class="fad fa-plus-circle"></i> Add news</button>
                        </div>
                    </form>
                    <?php
                }

                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>
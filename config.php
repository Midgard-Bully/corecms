<?php
//site config
session_start();
ob_start();


include('dbconfig.php');

//do not edit below this line unless you know what are you doing :) :P

$mysqliA = new mysqli("$sql_host", "$sql_user", "$sql_pass", "$sql_account_db");
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$mysqliC = new mysqli("$sql_host", "$sql_user", "$sql_pass", "$sql_character_db");
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$mysqliW = new mysqli("$sql_host", "$sql_user", "$sql_pass", "$sql_world_db");
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

if ($multirealm == 1)
{
    $mysqliSC = new mysqli("$sql_host", "$sql_user", "$sql_pass", "$sql_sl_char_db");
    /* check connection */
    if(mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    $mysqliSW = new mysqli("$sql_host", "$sql_user", "$sql_pass", "$sql_sl_world_db");
    /* check connection */
    if(mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
} elseif ($multirealm == 0)
{

} else {
    printf("Wrong Multirealm config. <br> Please set it to 1 or 0");
    exit();
}

$site_config = $mysqliA->query("SELECT * FROM `site_settings`") or die (mysqli_error($mysqliA));
while($pp_ress = $site_config->fetch_assoc())
{
    $paypal_client_id = $pp_ress['paypal_client_id'];
    $paypal_client_secret = $pp_ress['paypal_client_secret'];
    $paypal_return_url = $pp_ress['paypal_return_url'];
    $paypal_cancel_url = $pp_ress['paypal_cancel_url'];
    $paypal_currency = $pp_ress['paypal_currency'];
    $paypal_status = $pp_ress['paypal_status'];
    $site_name = $pp_ress['site_name'];
    $site_don = $pp_ress['site_en_don'];
    $site_description = $pp_ress['site_description'];
    $site_contact = $pp_ress['site_contact'];
    $site_realm = $pp_ress['site_realm'];
    $server_discord = $pp_ress['site_discord'];
    $site_theme = $pp_ress['site_theme'];
    $site_dir = $pp_ress['site_subdirectory'];
}

//PayPal config
require_once "vendor/autoload.php";

use Omnipay\Omnipay;

//go to https://developer.paypal.com/developer/applications/create and create a new app, it can take up to 3 hours for your app to work so please wait!
define("CLIENT_ID", "$paypal_client_id"); //your paypal client id
define("CLIENT_SECRET", "$paypal_client_secret"); //paypal client secret key

define("PAYPAL_RETURN_URL", "$paypal_return_url");
define("PAYPAL_CANCEL_URL", "$paypal_cancel_url");
define("PAYPAL_CURRENCY", "$paypal_currency"); // set your currency here EUR OR USD


$gateway = Omnipay::create('PayPal_Rest');
$gateway->setClientId(CLIENT_ID);
$gateway->setSecret(CLIENT_SECRET);

switch($paypal_status)
{
    case "true":
        $gateway->setTestMode(true);
        break;
    case "false":
        $gateway->setTestMode(false);
        break;
}

//total accounts
$acc_query = $mysqliA->query("SELECT `id` FROM `account`;");
$total_acc = $acc_query->num_rows;

$date_join = date('Y-m');
$new_acc_query = $mysqliA->query("SELECT `id` FROM `account` WHERE `joindate` LIKE '$date_join';");
$new_acc = $new_acc_query->num_rows;

$acc_on_q = $mysqliA->query("SELECT `id` FROM `account` WHERE `online` = '1';");
$online_acc = $acc_on_q->num_rows;

$paypal_query = $mysqliA->query("SELECT `id` FROM `payments`;");
$paypal_orders = $paypal_query->num_rows;

//News
$news_query = $mysqliA->query("SELECT * FROM `web_news`;") or die (mysqli_error($mysqliA));
$num_news = $news_query->num_rows;
<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-user-cog"></i> User Control Panel (UCP)</h2>
                        <p class="text-center">
                            <?php
								if (isset($_SESSION['id']))
								{
									session_destroy();
									echo '
                                        <div class="alert alert-warning" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> You are now logged out!
                                        </div>
                                    ';
									header('refresh:3; url=/');
								}
								else
								{
									echo '
                                        <div class="alert alert-warning" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> You are already logged out!
                                        </div>
                                    ';
								}
							?>
                        <br/>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <i class="fad fa-users"></i> Total accounts: <strong>23131</strong>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>